using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Centrale : MonoBehaviour
{
    // Start is called before the first frame update
    float time, timeA, timeS, timeP, timeI, timeC;
    float timeBegin;
    float timeEnd;

    int cpt_drone;
    
    public int[,] tabA = { { 578, 10, 660 },
    { 577, 9, 660 },{ 579, 9, 660 },
    { 576, 8, 660 },{ 580, 8, 660 }
    ,{ 575, 7, 660 },{ 581, 7, 660 }
    ,{ 575, 6, 660 },{ 576, 6, 660 },{ 577, 6, 660 },{ 578, 6, 660 },{ 579, 6, 660 },{ 580, 6, 660 },{ 581, 6, 660 }
    ,{ 575, 5, 660 },{ 581, 5, 660 }
    ,{ 575, 4, 660 },{ 578, 4, 660 },{ 581, 4, 660 }
    ,{ 575, 3, 660 },{ 581, 3, 660 }
    };

    public int[,] tabS = { { 576, 10, 660 },{ 577, 10, 660 },{ 578, 10, 660 },{ 579, 10, 660 },
    { 575, 9, 660 },{ 580, 9, 660 },
    { 580, 8, 660 },
    { 576,7, 660 },{ 577, 7, 660 },{ 578, 7, 660 },{ 579, 7, 660 },
    { 575, 6, 660 },
    { 575, 5, 660 },{ 579, 5, 660 },{ 580, 5, 660 },
    { 575, 4, 660 },{ 580, 4, 660 },
    { 576, 3, 660 },{ 577, 3, 660 },{ 578, 3, 660 },{ 579, 3, 660 }
      };

    public int[,] tabP = { { 575, 10, 660 },{ 576,10, 660 },{ 577,10, 660 }, { 578,10, 660 },{ 579,10, 660 },{ 580, 10, 660 },
    { 580, 9, 660 },{ 574, 9, 660 },
    { 580, 8, 660 },{ 574, 8, 660 },
    { 580, 7, 660 },{ 574, 7, 660 },
    { 580, 6, 660 },{ 579, 6, 660 },{ 578, 6, 660 },{ 577, 6, 660 },{ 576, 6, 660 },{ 575, 6, 660 },
    { 580, 5, 660 },
    { 580, 4, 660 },
    { 580, 3, 660 }
      };
    
    public int[,] tabI = { { 575, 10, 660 },{ 576,10, 660 },{ 577,10, 660 },{ 578, 10, 660 },{ 579, 10, 660 },{ 580, 10, 660 },{ 581, 10, 660 },
    { 578, 9, 660 },
    { 578, 8, 660 },
    { 578, 7, 660 },
    { 578, 6, 660 },
    { 578, 5, 660 },
    { 578, 4, 660 },
    { 578, 3, 660 },
    { 575, 2, 660 },{ 576, 2, 660 },{ 577, 2, 660 },{ 578, 2, 660 },{ 579, 2, 660 },{ 580, 2, 660 },{ 581, 2, 660 }
      };

    public int[,] tabC = { { 577, 10, 660 },{ 578, 10, 660 },{ 579, 10, 660 },{ 580, 10, 660 },{ 581, 10, 660 },
    { 576, 9, 660 },{ 582, 9, 660 },
    { 575, 8, 660 },{ 583, 8, 660 },
    { 584, 7, 660 },
    { 584, 6, 660 },
    { 584, 5, 660 },
    { 575, 4, 660 },{ 583, 4, 660 },
    { 576, 3, 660 },{ 582, 3, 660 },
    { 577, 2, 660 },{ 578, 2, 660 },{ 579, 2, 660 },{ 580, 2, 660 },{ 581, 2, 660 }
      };


    void Start()
    {

        time = 0.0f;
        timeA = 12.0f;
        timeS = 17.0f;
        timeP = 22.0f;
        timeI = 27.0f;
        timeC = 32.0f;
        timeEnd = 37.0f;

    }

     // Update is called once per frame
    void Update()
    {

        time = time + 1.0f * Time.deltaTime;

        if(time >= timeA && time <= timeS){

            cpt_drone = 0;

            for(int i = 0; i < 21; i++){
                if(GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().isAlive != 0){
                    GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().sendMessage(new Vector3(tabA[cpt_drone,0],tabA[cpt_drone,1],tabA[cpt_drone,2]));
                    cpt_drone ++;
                }
            }
        }
        if(time >= timeS && time <= timeP){

            cpt_drone = 0;

            for(int i = 0; i < 21; i++){
                if(GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().isAlive != 0){
                    GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().sendMessage(new Vector3(tabS[cpt_drone,0],tabS[cpt_drone,1],tabS[cpt_drone,2]));
                    cpt_drone ++;
                }
            }
        }
        if(time >= timeP && time <= timeI){

            cpt_drone = 0;

            for(int i = 0; i < 21; i++){
                if(GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().isAlive != 0){
                    GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().sendMessage(new Vector3(tabP[cpt_drone,0],tabP[cpt_drone,1],tabP[cpt_drone,2]));
                    cpt_drone ++;
                }
            }
        }
        if(time >= timeI && time <= timeC){

            cpt_drone = 0;

            for(int i = 0; i < 21; i++){
                if(GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().isAlive != 0){
                    GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().sendMessage(new Vector3(tabI[cpt_drone,0],tabI[cpt_drone,1],tabI[cpt_drone,2]));
                    cpt_drone ++;
                }
            }
        }
        if(time >= timeC && time <= timeEnd){

            cpt_drone = 0;

            for(int i = 0; i < 21; i++){
                if(GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().isAlive != 0){
                    GameObject.FindWithTag("Drone"+i.ToString()).GetComponent<AnimContr>().sendMessage(new Vector3(tabC[cpt_drone,0],tabC[cpt_drone,1],tabC[cpt_drone,2]));
                    cpt_drone ++;
                }
            }
        }
    }
}