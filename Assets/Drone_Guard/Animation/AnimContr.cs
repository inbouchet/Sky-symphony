using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimContr : MonoBehaviour
{
   // Start is called before the first frame update
   public Animator anim;
   public Vector3 initial_pos;
   public Vector3 new_pos;
   
   float time;
   float timeEnd;

   public int isAlive = 1;


   void Start()
   {
      anim = GetComponent<Animator>();
      initial_pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
      new_pos = Vector3.zero;

      time = 0.0f;
      timeEnd = 37.0f;

   }

   // Update is called once per frame
   void Update()
   {
      time = time + 1.0f * Time.deltaTime;

      if(time >= timeEnd){
         transform.position = Vector3.MoveTowards(transform.position, initial_pos, 10 * Time.deltaTime);
      }
   }

   public void sendMessage(Vector3 pos)
   {
      Debug.Log(anim.tag + " received position " + pos);
      transform.position = Vector3.MoveTowards(transform.position, pos, 10 * Time.deltaTime);
   }
}
